<?php

namespace App\Repository\Example;

use App\Entity\Example\Product;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Symfony\Bridge\Doctrine\RegistryInterface;

class ProductRepository extends ServiceEntityRepository
{
    /**
     * @param RegistryInterface $registry [description]
     */
    public function __construct(RegistryInterface $registry)
    {
        parent::__construct($registry, Product::class);
    }

    /**
     * @return array data for products list
     */
    public function findProductForList(): array
    {
        $qb = $this->createQueryBuilder('p')
            ->orderBy('p.createDate', 'ASC')
            ->getQuery();

        return $qb->execute();
    }
}
