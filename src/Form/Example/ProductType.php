<?php
// src/Form/Example/ProductType.php
namespace App\Form\Example;

use App\Entity\Example\Product;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;


class ProductType extends AbstractType
{
    /**
     * @param  FormBuilderInterface $builder [description]
     * @param  array                $options [description]
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder->add('label')
                ->add('create', SubmitType::class);
    }

    /**
     * Automatics property resolver
     * @param  OptionsResolver $resolver
     * @return void
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => Product::class,
        ));
    }
}
