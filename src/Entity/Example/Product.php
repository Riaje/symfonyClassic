<?php

namespace App\Entity\Example;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * @ORM\Entity(repositoryClass="App\Repository\Example\ProductRepository")
 */
class Product
{
    public function __construct()
    {
        $this->createDate = new \DateTime();
    }

    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255)
     *
     * @Assert\Type("string")
     */
    private $label;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\Example\Tarif", mappedBy="product", cascade="remove")
     */
    private $tarifs;

    /**
     * @ORM\Column(type="datetime", nullable=true)
     *
     * @Assert\DateTime()
     */
    private $createDate;

    /**
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @return string
     */
    public function getLabel(): ?string
    {
        return $this->label;
    }

    /**
     * @param  string $label
     * @return self
     */
    public function setLabel(string $label): self
    {
        $this->label = $label;

        return $this;
    }

    /**
     * @return \DateTime
     */
    public function getCreateDate(): ?\DateTime
    {
        return $this->createDate;
    }

    /**
     * @param  \dateTime|string $createDate
     * @return self
     */
    public function setCreateDate($createDate): self
    {
        if (is_string($createDate) && new \DateTime($createDate) !== false) {
            $createDate = new \DateTime($createDate);
        }
        $this->createDate = $createDate;

        return $this;
    }

    /**
     * @return array
     */
    public function getTarifs()
    {
        return $this->tarifs;
    }

    /**
     * @param  array $tarifs
     * @return self
     */
    public function setTarifs(array $tarifs): self
    {
        $this->tarifs = $tarifs;

        return $this;
    }
}
