<?php

namespace App\Entity\Example;

use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="App\Repository\Example\TarifRepository")
 */
class Tarif
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $label;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $reference;

    /**
     * @ORM\Column(type="integer", nullable=true)
     */
    private $sellPrice;

    /**
     * @ORM\Column(type="integer", nullable=true)
     */
    private $buyPrice;

    /**
     * @ORM\Column(type="datetime", nullable=true)
     */
    private $expiredDate;


    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\Example\Product", inversedBy="tarifs", cascade="remove")
     * @ORM\JoinColumn(nullable=true)
     */
    private $product;

    public function getId()
    {
        return $this->id;
    }

    public function getLabel(): ?string
    {
        return $this->label;
    }

    public function setLabel(string $label): self
    {
        $this->label = $label;

        return $this;
    }

    public function getReference(): ?string
    {
        return $this->reference;
    }

    public function setReference(string $reference): self
    {
        $this->reference = $reference;

        return $this;
    }

    public function getSellPrice(): ?int
    {
        return $this->sellPrice;
    }

    public function setSellPrice(?int $sellPrice): self
    {
        $this->sellPrice = $sellPrice;

        return $this;
    }

    public function getBuyPrice(): ?int
    {
        return $this->buyPrice;
    }

    public function setBuyPrice(?int $buyPrice): self
    {
        $this->buyPrice = $buyPrice;

        return $this;
    }

    public function getExpiredDate(): ?\DateTimeInterface
    {
        return $this->expiredDate;
    }

    public function setExpiredDate(?\DateTimeInterface $expiredDate): self
    {
        $this->expiredDate = $expiredDate;

        return $this;
    }

    /**
     * @return Product
     */
    public function getProduct()
    {
        return $this->product;
    }

    /**
     * @param  Product $product
     * @return self
     */
    public function setProduct(Product $product): self
    {
        $this->product = $product;

        return $this;
    }
}
