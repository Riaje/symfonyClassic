<?php
// src/Controller/Example/Product/ProductController.php
namespace App\Controller\Example;

use Sensio\Bundle\FrameworkExtraBundle\Configuration\ParamConverter;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use App\Entity\Example\Product;
use App\Form\Example\ProductType;
use App\Service\Example\ProductService;

/**
 * default route product
 * @route("/example/product")
 */
class ProductController extends Controller
{
    /**
     * Default page for products, display list
     * @Route("/", name="app_example_product_index")
     */
    public function indexAction(ProductService $productService)
    {
        return $this->render('example/product/list.html.twig',
            [
                'title' => 'List of all products',
                'products'=> $productService->generateProductList()
            ]
        );
    }

    /**
     * @Route("/new", name="app_example_product_new")
     */
    public function newAction(Request $request, ProductService $productService)
    {
        $productForm = $this->createForm(ProductType::class, new Product());
        $productForm->handleRequest($request);

        if ($productForm->isSubmitted() && $productForm->isValid()) {
            //Retrieve product object
            $product = $productForm->getData();
            //Save product
            if ($productService->create($product)) {
                $this->addFlash(
                    'notice',
                    'Product '.$product->getLabel().' is create'
                );
            }

            return $this->redirectToRoute('app_example_product_index');
        }

        return $this->render('example/product/new.html.twig',
            [
                'title' => 'Create a new product',
                'productForm'=> $productForm->createView()
            ]
        );
    }

     /**
      * @Route("/delete/{product}", name="app_example_product_delete")
      * @ParamConverter("get", options={"id" = "product"})
      */
    public function deleteAction(Product $product, ProductService $productService)
    {
        if ($productService->delete($product)) {
            $this->addFlash(
                'notice',
                'Product '.$product->getLabel().' is create'
            );
        }

        return $this->redirectToRoute('app_example_product_index');
    }
}
