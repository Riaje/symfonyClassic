<?php
// src/Controller/Example/Product/TarifController.php
namespace App\Controller\Example;

use Sensio\Bundle\FrameworkExtraBundle\Configuration\ParamConverter;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use App\Service\Example\TarifService;
use App\Form\Example\TarifType;
use App\Entity\Example\Product;
use App\Entity\Example\Tarif;

/**
 * default route product
 * @route("/example/tarif")
 */
class TarifController extends Controller
{
    /**
     * Default page for tarifs, display list
     * @Route("/{product}", name="app_example_tarif_list")
     * @ParamConverter("get", options={"id" = "product"})
     */
    public function listAction(Product $product)
    {
        if (empty($product)) {
            return $this->redirectToRoute('app_example_product_index');
        }

        return $this->render('example/tarif/list.html.twig',
            [
                'title' => $product->getLabel().'tarifs list',
                'product'=> $product
            ]
        );
    }

    /**
     * @Route("/new/{product}", name="app_example_tarif_new")
     * @ParamConverter("get", options={"id" = "product"})
     */
    public function newAction(
        Product $product,
        Request $request,
        TarifService $tarifService
    ) {
        $tarifForm = $this->createForm(TarifType::class, new Tarif());
        $tarifForm->handleRequest($request);

        if ($tarifForm->isSubmitted() && $tarifForm->isValid()) {
            //Retrieve product object
            $tarif = $tarifForm->getData();
            $tarif->setProduct($product);
            if ($tarifService->create($tarif)) {
                $this->addFlash(
                    'notice',
                    'Tarif '.$tarif->getLabel().' is created'
                );
            }
        }
        return $this->render('example/tarif/new.html.twig',
            [
                'title' => $product->getLabel().'tarifs list',
                'product'=> $product,
                'tarifForm' => $tarifForm->createView()
            ]
        );
    }
}
