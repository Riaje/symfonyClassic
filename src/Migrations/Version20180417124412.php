<?php declare(strict_types = 1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Migrations\AbstractMigration;
use Doctrine\DBAL\Schema\Schema;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
class Version20180417124412 extends AbstractMigration
{
    public function up(Schema $schema)
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('CREATE TABLE tarifs (id INT AUTO_INCREMENT NOT NULL, product_id INT DEFAULT NULL, label VARCHAR(255) NOT NULL, reference VARCHAR(255) NOT NULL, sell_price INT DEFAULT NULL, buy_price INT DEFAULT NULL, expired_date DATETIME DEFAULT NULL, INDEX IDX_F9B8C4964584665A (product_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci ENGINE = InnoDB');
        $this->addSql('ALTER TABLE tarifs ADD CONSTRAINT FK_F9B8C4964584665A FOREIGN KEY (product_id) REFERENCES product (id)');
        $this->addSql('ALTER TABLE product CHANGE create_date create_date DATETIME DEFAULT NULL');
    }

    public function down(Schema $schema)
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('DROP TABLE tarifs');
        $this->addSql('ALTER TABLE product CHANGE create_date create_date DATETIME DEFAULT CURRENT_TIMESTAMP');
    }
}
