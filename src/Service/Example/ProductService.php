<?php
// src/Service//Example/productService.php
namespace App\Service\Example;

use Doctrine\ORM\EntityManagerInterface;
use App\Entity\Example\Product;

class ProductService
{
    /**
     * @var EntityManagerInterface
     */
    private $em;

    /**
     * @param EntityManagerInterface $em
     */
    public function __construct(EntityManagerInterface $em)
    {
        $this->em = $em;
    }
    /**
     * Save a new product
     * @param  Product $product
     * @return bool
     */
    public function create(Product $product)
    {
        try {
            $this->em->persist($product);
            $this->em->flush();
        } catch (Exception $e) {
            throw new \Exception('Create product error: '.$e->getMessage());
        }

        return true;
    }

    /**
     * @return array
     */
    public function generateProductList()
    {
        $repository = $this->em->getRepository(Product::class);

        return $repository->findProductForList();
    }

    /**
     * @param  Product $product
     * @return [type]
     */
    public function delete(Product $product)
    {
        try {
            $this->em->remove($product);
            $this->em->flush();
        } catch (Exception $e) {
            throw new \Exception('Delete product error: '.$e->getMessage());
        }

        return true;
    }
}
