<?php
// src/Service//Example/productService.php
namespace App\Service\Example;

use Doctrine\ORM\EntityManagerInterface;
use App\Entity\Example\Tarif;
use App\Entity\Example\Product;

class TarifService
{
    /**
     * @var EntityManagerInterface
     */
    private $em;

    /**
     * @param EntityManagerInterface $em
     */
    public function __construct(EntityManagerInterface $em)
    {
        $this->em = $em;
    }
    /**
     * Save a new tarif with product link
     * @param  Product $product
     * @return bool
     */
    public function create(Tarif $tarif)
    {
        if (empty($tarif->getProduct())) {
            throw new \Exception("Error, tarif don't have product");
        }
        try {
            $this->em->persist($tarif);
            $this->em->flush();
        } catch (Exception $e) {
            throw new \Exception('Create tarif error: '.$e->getMessage());
        }

        return true;
    }

    /**
     * @return array
     */
    public function generateProductList()
    {
        $repository = $this->em->getRepository(Product::class);

        return $repository->findProductForList();
    }
}
